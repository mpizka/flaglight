# demonstrate the custom type functionality
# ex invocation: $ python3 example3.py -json '{"Hello": "World", "count": [1,2,3]}'

import flaglight as flag
import json

a = flag.Bool("a", help="boolean flag a")

# here we define a custom flag parsing stringified json values
j = flag.Custom("json", type=json.loads, default={}, help="json parseable")

flag.Parse()

print(f"-a:{a.Val} -json:{j.Val}")


