import flaglight as flag
from sys import argv, exit

flagsets = {
    "delete": flag.Args(),
    "make": flag.Args(),
}

# setup flagset subcommand 'delete'
flagsets["delete"].Bool("f", help="force delete")
flagsets["delete"].String("file", help="file to delete")
flagsets["delete"].Bool("h", help="show help")

# setup flagset subcommand 'make'
flagsets["make"].Bool("b", help="binary file")
flagsets["make"].Bool("n", help="no clobber")
flagsets["make"].String("file", help="filename")
flagsets["make"].Bool("h", help="show help")

# get cmd and parse argv[2;] into flagset
if not argv[1:] or argv[1] not in flagsets:
    print("avail. subcommands: delete, make")
    exit(1)
cmd = argv[1]
fs = flagsets[cmd]
fs.Parse(argv[2:])

# display help if required
if fs.Get("h").Set:
    print(fs.Help())
    exit(0)

if cmd == "delete":
    print(f"-f: {fs.Get('f').Val} -file: {fs.Get('file').Val}")
elif cmd == "make":
    print(f"-b: {fs.Get('b').Val} -n: {fs.Get('n').Val} -file: {fs.Get('file').Val}")

