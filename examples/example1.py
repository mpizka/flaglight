import flaglight as flag

a = flag.Bool("a")
b = flag.Bool("b")
c = flag.Bool("c")
fl = flag.Float("fl", default=42.2)
st = flag.String("foo")

flag.Parse()

print(f"-a: {a.Val} -b: {b.Val} -c: {c.Val} -fl: {fl.Val} -st: {foo.Val} tail: {flag.Tail()}")
