# About

flaglight provides capabilities to define groups of command-line argument flags and fill them in by parsing  argv or other lists of strings.

It is designed after the style of the [flagspackage][flags] package in golang. It tries to be simple and non-verbose.


# Introductory example

```python
import flaglight as flag
from monty import ThePub

serve_spam = flag.Bool("s", "serve spam")
serve_eggs = flag.Bool("e", "serve eggs")
nVikings   = flag.Int("vikings", "number of chanting vikings", default=4)
loudness   = flag.Float("db", "loudness of chanting (in Bel)", default=5.2)
pubname    = flag.String("name", "name of pub")

flag.Parse()

if not pubname.Set:
    raise Exception("Pub needs a name")

r = ThePub(name=pubname.Val, guests=nVikings.Val)
r.hasSpam(serve_spam.Val)
r.hasEggs(serve_eggs.Val)
r.chant(loudness.Val, words=flag.Tail())
```

An invocation of this program  may look like this:\
`$ pub.py -name "The Loud Viking" spam eggs ham and spam`\
to chant the words "spam eggs ham and spam" with 4 vikings at 5.2 Bel, while serving neither spam nor eggs.


# Usage

flaglight operates on _flagsets_ which are instances of its `Args()` class.


## Setting Flags

Argument sets offer the methods `Bool`, `Int`, `Float`, `String` and to set flags of the corresponding type, all of which accept the same arguments:

`Args.Bool(name, default=None, help=None) -> arg`

`name` determines the string signifying the flag, without the preceeding `-`.\
`default` determines the flags default-value.\
`help` determines the flags helptext (see [Args.Help()](#help))


## Setting Custom Flags

Via the `Custom` Method, flagsets can accept flags of a custom type.

`Args.Custom(name, type=str, default="", help=None)`

`type` is a callable taking a string as argument, and returning an object. If the string is not parseable into the object of that type, the callable should raise an Exception. The default should be an instance of the object.

An demonstration can be found in [example3](examples/example3.py)


## Parsing Arguments

Argument lists (represented by a `[str]`) can be parsed into the flasgset by calling its parse method:

`Args.Parse(a=sys.arv[1:])`

This method will raise a value-error if a the value after the flag signifier does not match its type, eg. if `-fl` was determined as a Float-Flag and the invocation is `... -fl hello`.


## Getting Flags

The `arg` object returned by the flag setting methods, represents the Flag. It offers the following fields to determine the flags state after [parsing](#parsing-arguments) the flagset.

`arg.Set` determines if the flag was set\
`arg.Val` holds the flags value if it was set

If the return of setting method was not stored, it can be accessed with the flagsets `Get` method:

`Args.Get(name)`

It returnd `None` if the flag so named doesn't exist in the flagset.

All non-parsed flags may be retreived by calling the flagsets `Tail()` method.


## Help

The Flagsets `Help()` Methods returns a helpstring of all its flags.


## Default Flagset

For convenience, The module instanciates a default Flagset which can be used with corresponding methods at module level.


# Examples

[example1](examples/example.py) illustrates usage of the default-flagset.

[example2](examples/example2.py) illustrates using multiple flagsets to implement subcommands with different command line flags.

[example3](examples/example3.py) demonstrates the usage of custom type flags.


# Contact

Contact me via protonmail: `mpizka@pm.me`

If you found flaglight useful, feel free to leave a star :smile:





[flags]: https://pkg.go.dev/flag
