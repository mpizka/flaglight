# module provides simplified argument parsing capabilities

import sys

# Args represents an argument list against which a list of arguments is being parsed
class Args:
    def __init__(self):
        # argument names (keys to arguments in order of adding)
        self.argumentNames = []
        # argument store, using names as keys
        self.arguments = {}
        # leftover arguments
        self.leftover = []

    # addArg adds an argument when called from the public methods
    def addArg(self, type, name, default, help):
        if name in self.arguments:
            raise ValueError("argument '{name}' already defined")
        a = arg(type, name, default, help)
        a.Val = default
        self.arguments[name] = a
        self.argumentNames.append(name)
        return a

    # Define a new Argument of type bool
    def Bool(self, name, help=None, default=False):
        return self.addArg(bool, f"-{name}", default, help)

    # Define a new Argument of type int
    def Int(self, name, help=None, default=None):
        return self.addArg(int, f"-{name}", default, help)

    # Define a new Argument of type float
    def Float(self, name, help=None, default=None):
        return self.addArg(float, f"-{name}", default, help)

    # Define a new Argument of type string
    def String(self, name, help=None, default=None):
        return self.addArg(str, f"-{name}", default, help)

    # Define a new Argument of a custom type
    def Custom(self, name, type=str, help=None, default=None):
        return self.addArg(type, f"-{name}", default, help)

    # Get returns the argument object with `name` (without `-`)
    # if it exists, otherwise None
    def Get(self, name):
        n = f"-{name}"
        if n in self.arguments:
            return self.arguments[n]
        return None

    # Tail returns the arguments leftover from the parse
    def Tail(self):
        return self.leftover

    # Help returns the help-text for the arguments
    def Help(self):
        title = []
        text = []
        for name in self.argumentNames:
            a = self.arguments[name]
            if a.default == None:
                title.append(f"{a.name} {a.type.__name__}")
            else:
                title.append(f"[{a.name} {a.type.__name__}]")
            default = f"(default={a.default})" if a.default else ''
            text.append(f"\t{a.name} {a.help if a.help else ''} {default}")
        title = " ".join(title)
        return title + "\n" + "\n".join(text)


    # Parse the arguments given. If none are given, argv[1:] is used instead
    def Parse(self, args=sys.argv[1:]):
        p = parser(args)
        # initialize the parser
        p.readToken()


        while p.curToken != None:
            a = self.arguments.get(p.curToken)

            # switch on current token

            # current token is bool-flag
            if a != None and a.type == bool:
                a.Set = True
                a.Val = True

            # current token is non-bool-flag
            elif a != None:
                pt = p.peekToken()
                if pt != None and pt not in self.arguments:
                    a.Val = a.type(pt)
                    a.Set = True
                    p.readToken()

            # current token is not a flag
            else:
                self.leftover.append(p.curToken)

            p.readToken()


# arg represents a single argument given
class arg:
    def __init__(self, type, name, default, help):
        self.type = type
        self.name = name
        self.default = default
        self.help = help
        self.Set = False

        self.Val = None


# parser is a helper type to ease the parsing process
class parser:
    def __init__(self, tokens):
        self.tokens = tokens
        self.curToken = None
        self.curPos = 0
        self.peekPos = 0

    def readToken(self):
        if self.peekPos >= len(self.tokens):
            self.curToken = None
        else:
            self.curToken = self.tokens[self.peekPos]
        self.curPos = self.peekPos
        self.peekPos += 1

    def peekToken(self):
        if self.peekPos >= len(self.tokens):
            return None
        else:
            return self.tokens[self.peekPos]


# convenience-code so argd can be used at top-level, without having
# to declare your own Args object

_ARGS = Args()
def Bool(*a, **kw):
    return _ARGS.Bool(*a, **kw)
def Int(*a, **kw):
    return _ARGS.Int(*a, **kw)
def Float(*a, **kw):
    return _ARGS.Float(*a, **kw)
def String(*a, **kw):
    return _ARGS.String(*a, **kw)
def Custom(*a, **kw):
    return _ARGS.Custom(*a, **kw)
def Parse(a=sys.argv[1:]):
    _ARGS.Parse(a)
def Tail(*a, **kw):
    return _ARGS.Tail()
